# mTable

[下载地址](https://gitee.com/CZJpython/mTable/releases/download/V1.0.0/MyTable1.0.2%201.apk)

#### 介绍
MyTable 是一个用来记录关系的电子手账，你可以用他来记录跟伴侣的相关点滴，也可以用来记录跟家人相处的点滴，当然,，也可以用来记录客户信息等等~
因为是使用Flutter+sqlite开发的本地应用，功能完全不需要网络，所以你无需担心信息的泄露，保障了你的隐私安全。
并且应用是完全免费的，没有任何广告及收费功能。

![效果图](show.jpg)